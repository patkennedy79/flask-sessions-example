from flask import Flask, render_template, session, flash, abort, redirect, url_for
from os import getenv

# Create the Flask application
app = Flask(__name__)

# Check if the `SECRET_KEY` environment variable is set; if not, utilize a default value.
# NOTE: The secret key is used to cryptographically-sign the cookies used for storing
#       the session data.
app.secret_key = getenv('SECRET_KEY', default='BAD_SECRET_KEY')

toys = [{'index': 0, 'name': 'Toy Car', 'price': 8, 'filename': 'img/IMG_7141.JPG'},
        {'index': 1, 'name': 'Dog Stuffed Animals', 'price': 10, 'filename': 'img/IMG_7151.JPG'},
        {'index': 2, 'name': 'Garbage Truck', 'price': 14, 'filename': 'img/IMG_7172.JPG'},
        {'index': 3, 'name': 'Crane Truck', 'price': 6, 'filename': 'img/IMG_7180.JPG'},
        {'index': 4, 'name': 'Toy House', 'price': 12, 'filename': 'img/IMG_7194.JPG'},
        {'index': 5, 'name': 'Fire Truck', 'price': 9, 'filename': 'img/IMG_7205.JPG'}]


@app.before_request
def app_before_request():
    if 'shopping_cart' not in session:
        # When creating a new dictionary object in the session object,
        # the new dictionary will be automatically detected.
        session['shopping_cart'] = [0 for _ in range(len(toys))]


@app.route('/')
def index():
    total_items_in_cart = 0

    for toy_index in range(len(toys)):
        total_items_in_cart += session['shopping_cart'][toy_index]

    return render_template('index.html', toys=toys, cart_total=total_items_in_cart)


@app.route('/shopping_cart')
def shopping_cart():
    total_items_in_cart = 0
    cart_total_price = 0

    for toy_index in range(len(toys)):
        total_items_in_cart += session['shopping_cart'][toy_index]
        cart_total_price += toys[toy_index]['price'] * session['shopping_cart'][toy_index]

    return render_template('shopping_cart.html',
                           toys=toys,
                           cart_total=total_items_in_cart,
                           cart_total_price=cart_total_price)


@app.route('/items/<int:id>/add_to_cart')
def add_item_to_cart(id):
    if id >= len(toys):
        abort(404)

    if session['shopping_cart'][id] < 10:
        # When modifying a mutable type (such as a list) within the session,
        # the `modified` parameter must be set to indicate a change to the
        # session (and subsequently a change to the underlying cookie sent
        # to the web browser).
        session['shopping_cart'][id] += 1
        session.modified = True
    else:
        flash('Maximum number (10) of specified item in shopping cart!', 'error')

    return redirect(url_for('index'))


@app.route('/empty_shopping_cart')
def empty_shopping_cart():
    # Clearing the `shopping_cart' list via a re-initialization
    # is automatically detected by the session.
    session['shopping_cart'] = [0 for _ in range(len(toys))]
    flash('Your shopping cart is now empty!', 'warning')
    return redirect(url_for('index'))


@app.route('/checkout')
def checkout():
    # Clearing the `shopping_cart' list via a re-initialization
    # is automatically detected by the session.
    session['shopping_cart'] = [0 for _ in range(len(toys))]
    flash('Thank you for your purchase! Your shopping cart is now empty again!', 'success')
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run()
