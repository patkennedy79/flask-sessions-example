## Overview

This Flask application demonstrates how [sessions](https://flask.palletsprojects.com/en/1.1.x/api/#sessions) work
in [Flask](https://flask.palletsprojects.com/en/1.1.x/) by building an e-commerce toy store:

![Flask Toy Store](static/img/flask_sessions_example_screenshot.png?raw=true "Flask Toy Store")

## Installation Instructions

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/flask-sessions-example.git
```

Create a new virtual environment:

```sh
$ cd flask-sessions-example
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

## Run the Development Server

In the top-level directory, set the file that contains the Flask application and specify that the development environment should be used:


```sh
(venv) $ export FLASK_APP=app.py
(venv) $ export FLASK_ENV=development
```

Run development server to serve the Flask application:

```sh
(venv) $ flask run
```

Navigate to 'http://localhost:5000' to view the website!

## Key Python Modules Used

* Flask - micro-framework for web application development
* Jinga - templating engine

This application is written using Python 3.9.0.

## Testing

To run all the tests:

```sh
(venv) $ pytest -v
```

To check the code coverage of the tests:

```sh
(venv) $ pytest --cov-report term-missing --cov=app
```
