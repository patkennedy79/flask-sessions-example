"""
This file (test_app.py) contains the functional tests for the app.py file.
"""
from flask import session
from app import toys

toy_titles = [b'Toy Car', b'Dog Stuffed Animals', b'Garbage Truck',
              b'Crane Truck', b'Toy House', b'Fire Truck']


def test_index_page(test_client):
    """
    GIVEN a Flask application
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """

    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Flask Toy Store' in response.data
    assert b'Toys' in response.data
    assert b'Cart' in response.data
    for title in toy_titles:
        assert title in response.data


def test_add_item_to_cart_valid(test_client):
    """
    GIVEN a Flask application
    WHEN the multiple items are added to the shopping cart
    THEN check the response is valid and the session is updated as expected
    """
    for item_index in range(len(toys)):
        for _ in range(item_index + 2):
            response = test_client.get(f'/items/{item_index}/add_to_cart', follow_redirects=True)
            assert response.status_code == 200
            assert b'Flask Toy Store' in response.data
            assert b'Maximum number (10) of specified item in shopping cart!' not in response.data

    assert session['shopping_cart'] == [2, 3, 4, 5, 6, 7]


def test_add_item_to_cart_exceed_max(test_client):
    """
    GIVEN a Flask application
    WHEN the multiple items are added to the shopping cart
    THEN check the response is valid and the session is updated as expected
    """
    for _ in range(10):
        response = test_client.get(f'/items/4/add_to_cart', follow_redirects=True)
        assert response.status_code == 200
        assert b'Flask Toy Store' in response.data
        assert b'Maximum number (10) of specified item in shopping cart!' not in response.data

    assert session['shopping_cart'] == [0, 0, 0, 0, 10, 0]

    for _ in range(4):
        response = test_client.get(f'/items/4/add_to_cart', follow_redirects=True)
        assert response.status_code == 200
        assert b'Flask Toy Store' in response.data
        assert b'Maximum number (10) of specified item in shopping cart!' in response.data

    assert session['shopping_cart'] == [0, 0, 0, 0, 10, 0]


def test_add_item_to_cart_invalid_id(test_client):
    """
    GIVEN a Flask application
    WHEN the '/items/723/add_to_cart' page is requested (GET)
    THEN check that a 404 (Not Found) error is returned
    """
    response = test_client.get(f'/items/723/add_to_cart', follow_redirects=True)
    assert response.status_code == 404
    assert b'Flask Toy Store' not in response.data


def test_shopping_cart_valid(test_client, fill_shopping_cart):
    """
    GIVEN a Flask application with a filled shopping cart
    WHEN the multiple items are added to the shopping cart
    THEN check the shopping cart is displayed properly
    """
    response = test_client.get('/shopping_cart', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Toy Store' in response.data
    assert b'Shopping Cart' in response.data
    for title in toy_titles:
        assert title in response.data

    total = 0
    for toy_index in range(len(toys)):
        total += toys[toy_index]['price'] * session['shopping_cart'][toy_index]
    assert bytes(str(total), 'utf') in response.data


def test_empty_shopping_cart(test_client, fill_shopping_cart):
    """
    GIVEN a Flask application with a filled shopping cart
    WHEN the '/empty_shopping_cart' page is requested (GET)
    THEN check the shopping cart is emptied
    """
    response = test_client.get('/empty_shopping_cart', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Toy Store' in response.data
    assert b'Shopping Cart' not in response.data
    assert b'Your shopping cart is now empty!' in response.data
    assert session['shopping_cart'] == [0, 0, 0, 0, 0, 0]


def test_checkout(test_client, fill_shopping_cart):
    """
    GIVEN a Flask application with a filled shopping cart
    WHEN the '/checkout' page is requested (GET)
    THEN check the shopping cart is emptied
    """
    response = test_client.get('/checkout', follow_redirects=True)
    assert response.status_code == 200
    assert b'Flask Toy Store' in response.data
    assert b'Shopping Cart' not in response.data
    assert b'Thank you for your purchase! Your shopping cart is now empty again!' in response.data
    assert session['shopping_cart'] == [0, 0, 0, 0, 0, 0]
