import pytest
from flask import session
from app import app, toys


@pytest.fixture(scope='function')
def test_client():
    # Create a test client using the Flask application configured for testing
    with app.test_client() as testing_client:
        yield testing_client  # this is where the testing happens!


@pytest.fixture(scope='function')
def fill_shopping_cart(test_client):
    for item_index in range(len(toys)):
        for _ in range(item_index + 4):
            response = test_client.get(f'/items/{item_index}/add_to_cart', follow_redirects=True)
            assert response.status_code == 200

    assert session['shopping_cart'] == [4, 5, 6, 7, 8, 9]
